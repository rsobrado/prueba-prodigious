'use strict';


(function() {


    var imagesJson = [];
    var imagesLength = 0;
    var currentImage = 0;


    function loadJSON(callback) {

        var xobj = new XMLHttpRequest();
        xobj.overrideMimeType('application/json');
        xobj.open('GET', '/scripts/gallery_json.json', true);

        xobj.onreadystatechange = function() {
            if (xobj.readyState === 4) {
                callback(xobj.responseText);
            }
        };
        xobj.send(null);

    }


    function refreshImage(imageData) {

        document.getElementById('currentImage').src = '/images/originals/' + imageData.image;
        document.getElementById('currentTittle').innerHTML = imageData.title;
        document.getElementById('currentDescription').innerHTML = 'Taken at the Intel Conference in ' + imageData.location + ' on ' + imageData.date;

    }


    function buildThumbs(imagesData) {

        var thumbs = imagesData.map(function(image, index) {

            return (
                '<a onclick="setCurrentImage(' + index + ')" href="#"><img class="thumb" src=/images/thumbs/' + image.thumbUrl + ' alt=""></a>'
            );
        });
        return thumbs;
    }


    //
    // index.html functions


    window.setCurrentImage = function(index) {

        console.log(currentImage);
        console.log(imagesJson);

        if (imagesJson.photos) {
            refreshImage(imagesJson.photos[index]);
        }

    };


    window.changeCurrentImage = function(plusIndex) {

        currentImage += plusIndex;

        if (currentImage >= imagesLength) {
            currentImage = 0;
        }

        if (currentImage < 0) {
            currentImage = imagesLength - 1;
        }

        console.log(currentImage);
        console.log(imagesJson);

        if (imagesJson.photos) {
            refreshImage(imagesJson.photos[currentImage]);
        }

    };


    //
    // Main


    window.changeCurrentImage(0);


    loadJSON(function(response) {

        imagesJson = JSON.parse(response);
        imagesLength = imagesJson.photos.length;

        document.getElementById('albumTittle').innerHTML = imagesJson.album.name;
        refreshImage(imagesJson.photos[currentImage]);
        document.getElementById('albumThumbs').innerHTML = buildThumbs(imagesJson.photos);

    });


})();
